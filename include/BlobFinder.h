#pragma once


#include "opencv2/features2d.hpp"
#include "opencv2/core.hpp"
#include "CinderOpenCV.h"
using namespace cv;


namespace po {

	struct Center
	{
		Point2d location;
		double radius;
		double confidence;
		float rotationAngle;
	};

	const std::vector<cv::Vec4i> blobHierarchy;

	class BlobFinder : public cv::SimpleBlobDetector{
		std::vector < std::vector<Center> > centers;
		SimpleBlobDetector::Params params;
	
	public:
		
		BlobFinder(const SimpleBlobDetector::Params &parameters = SimpleBlobDetector::Params()) {
			this->params = parameters;
		}

		void findBlobs(InputArray _image, InputArray _binaryImage, std::vector<Center> &centers, std::vector < std::vector<Point> > & contours) const
		{
			
		
			Mat image = _image.getMat(), binaryImage = _binaryImage.getMat();
			(void)image;
			centers.clear();
 
		
			//std::vector < std::vector<Point> > contours;
			Mat tmpBinaryImage = binaryImage.clone();
			findContours(tmpBinaryImage, contours, blobHierarchy,RETR_TREE,CHAIN_APPROX_SIMPLE);
			

			for (size_t contourIdx = 0; contourIdx < contours.size(); contourIdx++)
			{
				auto contour = contours[contourIdx];
				Center center;
				center.confidence = 1;
				Moments moms = moments(Mat(contours[contourIdx]));

				// find rotation
				cv::RotatedRect rotated = cv::minAreaRect(contour);
				center.rotationAngle = rotated.angle;


				if (params.filterByArea)
				{
					double area = moms.m00;
					if (area < params.minArea || area >= params.maxArea)
						continue;
				}

				if (params.filterByCircularity)
				{
					double area = moms.m00;
					double perimeter = arcLength(Mat(contours[contourIdx]), true);
					double ratio = 4 * CV_PI * area / (perimeter * perimeter);
					if (ratio < params.minCircularity || ratio >= params.maxCircularity)
						continue;
				}

				if (params.filterByInertia)
				{
					double denominator = std::sqrt(std::pow(2 * moms.mu11, 2) + std::pow(moms.mu20 - moms.mu02, 2));
					const double eps = 1e-2;
					double ratio;
					if (denominator > eps)
					{
						double cosmin = (moms.mu20 - moms.mu02) / denominator;
						double sinmin = 2 * moms.mu11 / denominator;
						double cosmax = -cosmin;
						double sinmax = -sinmin;

						double imin = 0.5 * (moms.mu20 + moms.mu02) - 0.5 * (moms.mu20 - moms.mu02) * cosmin - moms.mu11 * sinmin;
						double imax = 0.5 * (moms.mu20 + moms.mu02) - 0.5 * (moms.mu20 - moms.mu02) * cosmax - moms.mu11 * sinmax;
						ratio = imin / imax;
					}
					else
					{
						ratio = 1;
					}

					if (ratio < params.minInertiaRatio || ratio >= params.maxInertiaRatio)
						continue;

					center.confidence = ratio * ratio;
				}

				if (params.filterByConvexity)
				{
					std::vector < Point > hull;
					convexHull(Mat(contours[contourIdx]), hull);
					double area = contourArea(Mat(contours[contourIdx]));
					double hullArea = contourArea(Mat(hull));
					double ratio = area / hullArea;
					if (ratio < params.minConvexity || ratio >= params.maxConvexity)
						continue;
				}

				if (moms.m00 == 0.0)
					continue;
				center.location = Point2d(moms.m10 / moms.m00, moms.m01 / moms.m00);

				if (params.filterByColor)
				{
					if (binaryImage.at<uchar>(cvRound(center.location.y), cvRound(center.location.x)) != params.blobColor)
						continue;
				}

				//compute blob radius
				{
					std::vector<double> dists;
					for (size_t pointIdx = 0; pointIdx < contours[contourIdx].size(); pointIdx++)
					{
						Point2d pt = contours[contourIdx][pointIdx];
						dists.push_back(norm(center.location - pt));
					}
					std::sort(dists.begin(), dists.end());
					center.radius = (dists[(dists.size() - 1) / 2] + dists[dists.size() / 2]) / 2.;
				}

				centers.push_back(center);


#ifdef DEBUG_BLOB_DETECTOR
				//    circle( keypointsImage, center.location, 1, Scalar(0,0,255), 1 );
#endif
			}
#ifdef DEBUG_BLOB_DETECTOR
			//  imshow("bk", keypointsImage );
			//  waitKey();
#endif
		}
		void detect(InputArray image, std::vector<KeyPoint>& keypoints, std::vector < std::vector<Point> > &contours, InputArray mask = noArray())
		{



			//TODO: support mask
			keypoints.clear();
			Mat grayscaleImage;
			if (image.channels() == 3 || image.channels() == 4)
				cvtColor(image, grayscaleImage, COLOR_BGR2GRAY);
			else
				grayscaleImage = image.getMat();

			if (grayscaleImage.type() != CV_8UC1) {
				CV_Error(Error::StsUnsupportedFormat, "Blob detector only supports 8-bit images!");
			}

			std::vector < std::vector<Center> > centers;
			for (double thresh = params.minThreshold; thresh < params.maxThreshold; thresh += params.thresholdStep)
			{
				Mat binarizedImage;
				threshold(grayscaleImage, binarizedImage, thresh, 255, THRESH_BINARY);

				std::vector < Center > curCenters;
				findBlobs(grayscaleImage, binarizedImage, curCenters, contours);
				std::vector < std::vector<Center> > newCenters;
				for (size_t i = 0; i < curCenters.size(); i++)
				{
					bool isNew = true;
					for (size_t j = 0; j < centers.size(); j++)
					{
						double dist = norm(centers[j][centers[j].size() / 2].location - curCenters[i].location);
						isNew = dist >= params.minDistBetweenBlobs && dist >= centers[j][centers[j].size() / 2].radius && dist >= curCenters[i].radius;
						if (!isNew)
						{
							centers[j].push_back(curCenters[i]);

							size_t k = centers[j].size() - 1;
							while (k > 0 && centers[j][k].radius < centers[j][k - 1].radius)
							{
								centers[j][k] = centers[j][k - 1];
								k--;
							}
							centers[j][k] = curCenters[i];

							break;
						}
					}
					if (isNew)
						newCenters.push_back(std::vector<Center>(1, curCenters[i]));
				}
				std::copy(newCenters.begin(), newCenters.end(), std::back_inserter(centers));
			}

			for (size_t i = 0; i < centers.size(); i++)
			{
				if (centers[i].size() < params.minRepeatability)
					continue;
				Point2d sumPoint(0, 0);
				double normalizer = 0;
				float angle;
				for (size_t j = 0; j < centers[i].size(); j++)
				{
					sumPoint += centers[i][j].confidence * centers[i][j].location;
					normalizer += centers[i][j].confidence;
					angle = centers[i][j].rotationAngle;
				}
				sumPoint *= (1. / normalizer);
				KeyPoint kpt(sumPoint, (float)(centers[i][centers[i].size() / 2].radius) * 2.0f);
				kpt.angle = angle;
				keypoints.push_back(kpt);
			}
		}

	};


	static Ptr<BlobFinder> createDetector(const SimpleBlobDetector::Params &parameters = SimpleBlobDetector::Params()) {
		return makePtr<po::BlobFinder>(parameters);
	}
}

