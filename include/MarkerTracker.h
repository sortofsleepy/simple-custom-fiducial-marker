#pragma once

#include "CinderOpenCV.h"
#include "cinder/Vector.h"
#include "cinder/gl/Texture.h"
#include <vector>
namespace po {

	typedef struct {
		cv::Point center;
		int childCount = 0;
		int parentId;
		float angle = 0.0f;
		float width;
		float height;
		bool isRotated = false;
		int id;
	}BlobMarker;

	class MarkerTracker {

		// the distance before an object is considered rotated.
		int rotationDistance;

		//! Used to store current frame contours
		std::vector<std::vector<cv::Point> > contours;

		//! store the current hierarcy of all the contours
		std::vector<cv::Vec4i> hierarchy;

		//! Store all the current found markers
		std::vector<BlobMarker> markers;

		//! Store all contour indices that have children within them. 
		std::vector<int> indices;

		//! Erases vectors
		void clearVectors();

		//! Pre-processes images
		cv::Mat preprocessImage(cv::Mat img);

		//! checks to see if the object is rotated based on it's current angle.
		bool isObjectRotated(cv::RotatedRect rect);
		
		//! Returns the centroid for the current contour. 
		cv::Point2f getCentroid(std::vector<cv::Point> contour);

	public:
		MarkerTracker();

		float minBlobSize;
		float maxBlobSize;

		// textures for showing the camera feed
		ci::gl::TextureRef mCameraTexture;

		// texture for showing the processed camera feed
		ci::gl::TextureRef mThreshTexture;

		//! Updates the tracker with the new frame.
		void update(cv::Mat img);

		//! Returns all currently found markers
		std::vector<BlobMarker> getFoundMarkers();

		//! Draws camera feed and thresholded feed
		void drawLayers();

		//! Alternative to getFoundMarkers
		void draw(std::function<void(std::vector<BlobMarker>)> custom = nullptr);
	};
}