#include "cinder/app/App.h"
#include "cinder/app/RendererGl.h"
#include "cinder/gl/gl.h"
#include "CinderOpenCV.h"
#include "cinder/Log.h"
#include "KinectWrapper.h"
using namespace ci;
using namespace ci::app;
using namespace std;



typedef struct {

	cv::Point center;
	int childCount = 0;
	int parentId;
	float angle = 0.0f;


}BlobMarker;

class OpenCVTestApp : public App {
public:
	cv::Point2f getCentroid(vector<cv::Point> contour);
	void setup() override;
	void mouseDown(MouseEvent event) override;
	void update() override;
	void draw() override;
	void drawLayers();
	void updateMarkers();
	void updateTextures(cv::Mat edge, cv::Mat camera);

	vector<vector<cv::Point> > contours;
	vector<vec2> centers;
	vector<cv::Vec4i> hierarchy;
	vector<BlobMarker> markers;

	Surface data;

	KinectWrapper wrapper;
	bool cameraReady = false;

	gl::TextureRef mCameraTexture;
	gl::TextureRef mEdgeTexture;

};

void OpenCVTestApp::setup()
{
	wrapper.setup([this] {

		cameraReady = true;
	});



	data = Surface(loadImage(loadAsset("contourtest7.png")));
}

void OpenCVTestApp::mouseDown(MouseEvent event)
{
}


void OpenCVTestApp::update()
{
	if (cameraReady) {
		wrapper.update();
		updateMarkers();
	}
}

void OpenCVTestApp::updateTextures(cv::Mat edge, cv::Mat camera) {

	mCameraTexture = gl::Texture::create(fromOcv(camera));
	mEdgeTexture = gl::Texture::create(fromOcv(edge));
}
void OpenCVTestApp::drawLayers()
{

	gl::ScopedBlendAdditive add;
	gl::draw(mCameraTexture);


}
void OpenCVTestApp::draw()
{
	gl::clear(Color(0, 0, 0));

	drawLayers();

	gl::ScopedColor col(1, 0, 0);
	for (int i = 0; i < markers.size(); ++i) {
		auto marker = markers[i];

		//gl::drawSolidCircle(fromOcv(marker.center), 15);

		// draw all markers that have a child count greater than 0 
		if (marker.childCount > 0) {
			gl::drawSolidCircle(fromOcv(marker.center), 15);
		}
	}
}

cv::Point2f OpenCVTestApp::getCentroid(vector<cv::Point> contour) {
	// calculate centroids of contours so we 
	cv::Moments m = cv::moments(contour);
	auto center = cv::Point2f();
	if (m.m00 != 0) {

		center.x = m.m10 / m.m00;
		center.y = m.m01 / m.m00;

	}
	else {
		center.x = 0;
		center.y = 0;
	}

	return center;
}

void OpenCVTestApp::updateMarkers() {
	markers.clear();
	contours.clear();
	vector<int> indices;

	//cv::Mat img = toOcv(data);
	cv::Mat img = toOcv(wrapper.getIRData());

	cv::Mat gray;
	cv::cvtColor(img.clone(), gray, CV_BGR2GRAY);

	// Use Canny instead of threshold to catch squares with gradient shading
	cv::Mat bw, thresh;
	cv::GaussianBlur(gray, bw, cv::Size(7, 7), 3);

	cv::threshold(bw, thresh, 250, 255, cv::THRESH_BINARY);

	updateTextures(bw, thresh);

	// Find contours
	cv::findContours(thresh, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE);

	for (int i = 0; i < contours.size(); ++i) {

		auto contour = contours[i];

		// filter out non rectangular objects, assuming IR will be rectangle like.
		vector<cv::Point> approx;
		cv::approxPolyDP(cv::Mat(contours[i]), approx, cv::arcLength(cv::Mat(contours[i]), true)*0.04, true);


		BlobMarker marker;



		// ================ FIND CHILDREN ================= //

		// initial work to figure out children
		// if hierarchy index is -1, that means there isn't a parent.
		if (hierarchy[i][3] != -1 && hierarchy[i][2] == -1) {

			auto parent = hierarchy[i][3];
			indices.push_back(hierarchy[i][3]);
		}
		else if (hierarchy[i][3] == -1) {
			marker.center = getCentroid(contour);
		}

		markers.push_back(marker);


	}


	// loop through and see how many times a child appears in a parent. 
	// if it's > 1, we have a shape we want. 
	for (int i = 0; i < contours.size(); ++i) {
		int result = std::count(indices.begin(), indices.end(), i);
		if (result > 0) {
			markers[i].childCount = result;
		}
	}


	//======= DEBUGGING ===================== // 
	// check shapes
	int count = 0;

	for (BlobMarker & marker : markers) {
		if (marker.childCount > 0) {
			count++;
		}
	}

	//CI_LOG_I("Found " << count << " markers");

}

CINDER_APP(OpenCVTestApp, RendererGl)
