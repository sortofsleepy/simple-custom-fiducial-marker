#include "BlobTracker.h"
#include "cinder/Log.h"
#include "cinder/gl/gl.h"
using namespace ci;
using namespace std;

namespace po {
	BlobTracker::BlobTracker() :rotationDistance(55) {
	
		params.blobColor = 255;
		params.filterByColor = true;
		params.minThreshold = 100;
		params.maxThreshold = 255;
		params.filterByArea = true;
		params.minArea = 1;

		detector = po::createDetector(params);
	
	}

	vector<BlobMarker> BlobTracker::getFoundMarkers() {
		return markers;
	}

	void BlobTracker::update(cv::Mat img) {

		// ensure previous frame data cleared.
		clearVectors();

		cv::Mat gray;
		cv::cvtColor(img.clone(), gray, CV_BGR2GRAY);

		// Use Canny instead of threshold to catch squares with gradient shading
		cv::Mat bw, thresh;
		cv::GaussianBlur(gray, bw, cv::Size(5, 5), 3);

		cv::threshold(bw, thresh, 250, 255, cv::THRESH_BINARY);

		mCameraTexture = gl::Texture::create(fromOcv(img));
		mThreshTexture = gl::Texture::create(fromOcv(thresh));


		detector->detect(img,mKeyPoints, contours);

	}

	void BlobTracker::draw(std::function<void(vector<BlobMarker>)> func) {
		if (func != nullptr) {
			func(getFoundMarkers());
		}
	}

	void BlobTracker::drawLayers() {
		gl::ScopedBlendAdditive add;
		gl::draw(mCameraTexture);
		gl::draw(mThreshTexture);


	}

	// =========== PRIVATE =============== //

	bool BlobTracker::isObjectRotated(float angle) {
		// OpenCV has "negative" angles so normalize it so we are dealing with normal numbers
		if (angle < 0) {
			angle *= -1;
		}

		float distance = glm::distance(angle, 0.0f);

		if (distance > rotationDistance) {
			return true;
		}
		else {
			return false;
		}
	}

	// OpenCV throws an error when trying to use this method ? :/ 
	cv::Mat BlobTracker::preprocessImage(cv::Mat img) {
		cv::Mat gray;
		cv::cvtColor(img.clone(), gray, CV_BGR2GRAY);

		cv::Mat bw, thresh;
		cv::GaussianBlur(gray, bw, cv::Size(5, 5), 0);

		cv::threshold(bw, thresh, 250, 255, cv::THRESH_BINARY_INV);

		return img;
	}

	void BlobTracker::clearVectors() {
		contours.clear();
		markers.clear();
		mKeyPoints.clear();

	}

	cv::Point2f BlobTracker::getCentroid(vector<cv::Point> contour) {

		// calculate centroids of contours so we 
		cv::Moments m = cv::moments(contour);
		auto center = cv::Point2f();
		if (m.m00 != 0) {

			center.x = m.m10 / m.m00;
			center.y = m.m01 / m.m00;

		}
		else {
			center.x = 0;
			center.y = 0;
		}

		return center;

	}
}