#include "MarkerTracker.h"
#include "cinder/gl/gl.h"
#include "cinder/Log.h"
using namespace ci;
using namespace std;

namespace po {
	MarkerTracker::MarkerTracker() :rotationDistance(55),minBlobSize(20),maxBlobSize(100) {}

	vector<BlobMarker> MarkerTracker::getFoundMarkers() {
		return markers;
	}

	void MarkerTracker::update(cv::Mat img) {

		// ensure previous frame data cleared.
		clearVectors();

		cv::Mat gray;
		cv::cvtColor(img.clone(), gray, CV_BGR2GRAY);

		// Use Canny instead of threshold to catch squares with gradient shading
		cv::Mat bw, thresh;
		cv::GaussianBlur(gray, bw, cv::Size(3,3), 3);

	
		cv::Scalar offset(200,200,200);
		cv::Scalar targetColor(255, 255, 255);
		cv::inRange(bw, targetColor - offset, targetColor + offset, thresh);


		//cv::threshold(bw, thresh, 250, 255, cv::THRESH_BINARY);

		//cv::bitwise_not(thresh,thresh);

		mCameraTexture = gl::Texture::create(fromOcv(img));
		mThreshTexture = gl::Texture::create(fromOcv(thresh));


		// Find contours
		cv::findContours(thresh, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE);

		for (int i = 0; i < contours.size(); ++i) {

			auto contour = contours[i];

			// filter out non rectangular objects, assuming IR will be rectangle like.
			//vector<cv::Point> approx;
			//cv::approxPolyDP(cv::Mat(contours[i]), approx, cv::arcLength(cv::Mat(contours[i]), true)*0.04, true);


			BlobMarker marker;



			// ================ FIND CHILDREN ================= //

			// initial work to figure out children
			// hierarchy[i][3] indicates if the contour has a parent. 
			// hierarchy[i][2] indicates whether or not the contour has an immediate child. 
			//! If both of those things are -1, that means that that contour is a child of a larger one. 
			if (hierarchy[i][3] != -1 && hierarchy[i][2] == -1) {

				auto parent = hierarchy[i][3];
				indices.push_back(hierarchy[i][3]);
			}
			else if (hierarchy[i][3] == -1) {
				
				auto center = getCentroid(contour);

				if (center.x != 0 && center.y != 0) {
					marker.center = center;
				}
			}


			// =============== FIGURE OUT ROTATION ============== //
			cv::RotatedRect rect = cv::minAreaRect(contour);
			marker.isRotated = isObjectRotated(rect);


			// ensure the blob size is large enough to qualify as a possible marker
			// and try to filter out things that are too small. 
			float radius;
			cv::Point2f center;
			cv::minEnclosingCircle(contour, center, radius);

			if (radius > minBlobSize && radius < maxBlobSize) {
				markers.push_back(marker);
			}


		}


		// loop through and see how many times a child appears in a parent. 
		// if it's > 1, we have a shape we want(we set it to greater than 1 to ensure
		// that an outer contour doesn't get extracted, but it's possible this could be fixed by 
		// finding a min/max blob size). 
		for (int i = 0; i < markers.size(); ++i) {
			int result = std::count(indices.begin(), indices.end(), i);
			if (result > 1) {
				markers[i].childCount = result;
			}
		}

	}

	void MarkerTracker::draw(std::function<void(vector<BlobMarker>)> func) {
		if (func != nullptr) {
			func(getFoundMarkers());
		}
	}

	void MarkerTracker::drawLayers() {
		
		// draw layers for debugging. 
		gl::ScopedBlendAdditive add;
		gl::draw(mCameraTexture);
		//gl::draw(mThreshTexture);
		

	}

	// =========== PRIVATE =============== //

	bool MarkerTracker::isObjectRotated(cv::RotatedRect rect) {
		
		auto angle = rect.angle;
		if (rect.size.width < rect.size.height) {
			angle = 90 + angle;
		}

		float distance = glm::distance(angle, 0.0f);

	
		if (distance > 45) {

			return true;
		}
		else {
			return false;
		}

		

	}

	// OpenCV throws an error when trying to use this method ? :/ 
	cv::Mat MarkerTracker::preprocessImage(cv::Mat img) {
		cv::Mat gray;
		cv::cvtColor(img.clone(), gray, CV_BGR2GRAY);

		cv::Mat bw, thresh;
		cv::GaussianBlur(gray, bw, cv::Size(5, 5), 0);

		cv::threshold(bw, thresh, 250, 255, cv::THRESH_BINARY_INV);

		return img;
	}

	void MarkerTracker::clearVectors() {
		contours.clear();
		hierarchy.clear();
		markers.clear();
	}

	cv::Point2f MarkerTracker::getCentroid(vector<cv::Point> contour) {

		// calculate centroids of contours so we 
		cv::Moments m = cv::moments(contour);
		auto center = cv::Point2f();
		if (m.m00 != 0) {

			center.x = m.m10 / m.m00;
			center.y = m.m01 / m.m00;

		}
		else {
			center.x = 0;
			center.y = 0;
		}

		return center;

	}
}
