#include "cinder/app/App.h"
#include "cinder/app/RendererGl.h"
#include "cinder/gl/gl.h"
#include "CinderOpenCV.h"
#include "cinder/Log.h"
#include "KinectWrapper.h"
#include "MarkerTracker.h"
using namespace ci;
using namespace ci::app;
using namespace std;


typedef struct {

	gl::TextureRef rotated;
	gl::TextureRef default;

}AnimationItem;


class OpenCVTestApp : public App {
public:
	cv::Point2f getCentroid(vector<cv::Point> contour);
	void setup() override;
	void mouseDown(MouseEvent event) override;
	void update() override;
	void draw() override;
	void drawLayers();
	void updateMarkers();
	void updateTextures(cv::Mat edge, cv::Mat camera);

	vector<vector<cv::Point> > contours;
	vector<vec2> centers;
	vector<cv::Vec4i> hierarchy;
	//vector<BlobMarker> markers;


	po::MarkerTracker tracker;
	Surface data;

	KinectWrapper wrapper;
	bool cameraReady = false;

	gl::TextureRef mCameraTexture;
	gl::TextureRef mEdgeTexture;




	std::map<int, AnimationItem> animations;
};

void OpenCVTestApp::setup()
{
	wrapper.setup([this] {

		cameraReady = true;
	});

	AnimationItem item1;
	AnimationItem item2;

	item1.default = gl::Texture::create(loadImage(app::loadAsset("normal1.jpg")));
	item2.default = gl::Texture::create(loadImage(app::loadAsset("normal2.jpg")));
	item1.rotated = gl::Texture::create(loadImage(app::loadAsset("rotate1.jpg")));
	item2.rotated = gl::Texture::create(loadImage(app::loadAsset("rotate2.jpg")));

	animations[0] = item1;
	animations[1] = item2;


	data = Surface(loadImage(loadAsset("contourtest7.png")));
}

void OpenCVTestApp::mouseDown(MouseEvent event)
{
}


void OpenCVTestApp::update()
{
	if (cameraReady) {
		wrapper.update();
		tracker.update(toOcv(wrapper.getIRData()));
	}
}

void OpenCVTestApp::updateTextures(cv::Mat edge, cv::Mat camera) {

	mCameraTexture = gl::Texture::create(fromOcv(camera));
	mEdgeTexture = gl::Texture::create(fromOcv(edge));
}
void OpenCVTestApp::drawLayers()
{

	gl::ScopedBlendAdditive add;
	gl::draw(mCameraTexture);


}



void OpenCVTestApp::draw()
{
	gl::clear(Color(0, 0, 0));


	tracker.drawLayers();

	tracker.draw([=](vector<po::BlobMarker> markers) {

		for (po::BlobMarker &mk : markers) {

			if (mk.childCount > 1) {
				if (mk.isRotated) {
					gl::ScopedColor color(1, 1, 0);
					gl::drawSolidCircle(fromOcv(mk.center), 10);
				}
				else {
					gl::ScopedColor color(1, 0, 0);
					gl::drawSolidCircle(fromOcv(mk.center), 10);
				}
			}

		}

	});

}


void OpenCVTestApp::updateMarkers() {





}

CINDER_APP(OpenCVTestApp, RendererGl)
